.. This file is made to explain Volume Uncertainty. 


When the
sample volume and calibration volume are different, a linearity check should be performed to
calculate the relative deviation of the two volumes from linearity.
The
random volume error is covered by the measurement precision for measurement systems having
a flow controller that corrects for temperature and pressure (e.g. Mass Flow Controller, critical
orifices). Mass Flow Controllers are prone to drift, therefore it is important to take this into account
or to calibrate the measurement system regularly. For off-line sampling using sorbent tubes, the
uncertainty of the volume sampled depends on the sampling flow rate and sampling time, both
associated with uncertainties that need to be estimated.