
Bibliography
============

.. [Guidelines]  Updated Measurement Guideline for NOx and VOC  `reference file <https://www.actris.eu/sites/default/files/Documents/ACTRIS-2/Deliverables/WP3_D3.17_M42.pdf>`_


.. [UncertaintyGuide] `Guide to the Expression of Uncertainty in Measurement (JCGM 100, 2008) <https://www.bipm.org/documents/20126/2071204/JCGM_100_2008_E.pdf/cb0ef43f-baa5-11cf-3f85-4dcd86f77bd6>`_

.. [ACTRIS]  ACTRIS-2 `website <https://www.actris.eu/>`_

.. [EBAS]  `EBAS Database <https://ebas.nilu.no/>`_