About TUCAVOC
=============


TUCAVOC was developed within the framework of the European project
*Metrology for Climate Relevant VOCs*. 
The project 19ENV06 MetClimVOC has received funding from the 
EMPIR programme cofinanced by the Participating States and
from the European Union's Horizon 2020 research and innovation programme.
For further information: `<https://www.metclimvoc.eu>`_. 



Support 
-------

If you have any question or problem with TUCAVOC, 
please make sure that you do not find the answer on this documentation first.

Otherwise we are happy to provide you any support.

Bugs & Issues
-------------

If you find a bug or an issue, you can visit our 
`issue tracker <https://gitlab.com/empa503/atmospheric-measurements/tucavoc/-/issues>`_ .


.. _contact:

Contact
-------

`tucavoc official repository <https://gitlab.com/empa503/atmospheric-measurements/tucavoc>`_
is managed by 
the
`Group for Climate Gases <https://www.empa.ch/web/s503//climate-gases>`_
which is a group from the 
`Laboratory for Air Pollution / Environmental Technology from EMPA <https://www.empa.ch/web/empa/air-pollution-/-environmental-technology>`_


The prefered way of contacting us for bugs or new feature requests is by 
opening an issue on the 
`gitlab repository <https://gitlab.com/empa503/atmospheric-measurements/tucavoc/-/issues>`_.


Licence
-------

Copyright (C) 2022 abt503 / Climate Gases / EMPA


This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see `<https://www.gnu.org/licenses/>`_ .
